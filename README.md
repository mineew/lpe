# Lorem Picsum Explorer

"Lorem Picsum Explorer" is a small react app created exclusively for
educational purposes. With it, you can view the photos published on the
[Lorem Picsum](https://picsum.photos/).

![Screenshot](/screenshot.png)

## Install and run

To install and run the application, type the following in your terminal:

```
~ $ mkdir lpe && cd lpe
~ $ git clone https://gitlab.com/mineew/lpe.git .
~ $ npm install
~ $ npm run start
```

Then, in your browser, go to [http://localhost:3000/](http://localhost:3000/).

## All available scripts

| NPM Script          | Description                             |
| ------------------- | --------------------------------------- |
| `npm run start`     | runs the application                    |
| `npm run test`      | tests the application                   |
| `npm run cover`     | shows the code coverage                 |
| `npm run lint -s`   | lints the code                          |
| `npm run build`     | creates a production build              |
| `npm run analyze`   | analyzes code bloat through source maps |
| `npm run clear`     | removes the temporary folders           |
| `npm run storybook` | runs the UI development environment     |

## Frameworks and tools

This application is built with the following frameworks and tools:

* [React](https://reactjs.org/) - A JavaScript library for building user
  interfaces;
* [Create React App](https://github.com/facebook/create-react-app) - create
  react apps with no build configuration;
* [Blueprint](https://blueprintjs.com/) - a React-based UI toolkit for the web.

The tests are written with these tools:

* [Jest](https://jestjs.io/) - Delightful JavaScript Testing;
* [Enzyme](http://airbnb.io/enzyme/) - a JavaScript Testing utility for React;
* [Storybook](https://storybook.js.org/) - the UI Development Environment.

The code style is managed by:

* [ESlint](https://eslint.org/) - The pluggable linting utility for JavaScript
  and JSX;
* [eslint-config-standard](https://github.com/standard/eslint-config-standard) -
  ESLint Config for JavaScript Standard Style.

Other tools worth mentioning:

* [clipboard-copy](https://github.com/feross/clipboard-copy) - lightweight copy
  to clipboard for the web;
* [keycode-js](https://github.com/kabirbaidhya/keycode-js) - a JavaScript
  package with Key Code constants;
* [ultimate-pagination](https://github.com/ultimate-pagination/ultimate-pagination) -
  universal pagination model generation algorithm that can be used to build
  a UI component;
* [core-js](https://github.com/zloirock/core-js/) - modular standard
  library for JavaScript;
* [whatwg-fetch](https://github.com/github/fetch) - a `window.fetch` JavaScript
  polyfill.

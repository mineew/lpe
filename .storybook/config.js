import React from 'react'
import { configure, addDecorator } from '@storybook/react'
import { setOptions } from '@storybook/addon-options'
import { withKnobs } from '@storybook/addon-knobs'
import '@storybook/addon-console'

import 'normalize.css/normalize.css'
import '@blueprintjs/core/lib/css/blueprint.css'
import '@blueprintjs/icons/lib/css/blueprint-icons.css'

const IFRAME_MARGIN = 20

addDecorator((story) => (
  <div style={{ margin: IFRAME_MARGIN }}>
    {story()}
  </div>
))

addDecorator(withKnobs)

const customRequire = require.context(
  '../src/components', true, /\.stories\.js$/
)

function loadStories () {
  customRequire.keys().forEach(
    (filename) => customRequire(filename)
  )
}

configure(loadStories, module)

setOptions({ addonPanelInRight: true })

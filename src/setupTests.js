import { configure } from 'enzyme'
import Adapter from 'enzyme-adapter-react-16'
import 'jest-localstorage-mock'

configure({ adapter: new Adapter() })

// Blueprint's <Popover /> uses Popper.js
// This is necessary for testing the <Dropdown /> component
// https://github.com/FezVrasta/popper.js/issues/478#issuecomment-407422016
if (global.document) {
  document.createRange = () => ({
    setStart: () => {},
    setEnd: () => {},
    commonAncestorContainer: {
      nodeName: 'BODY',
      ownerDocument: document
    }
  })
}

// This is necessary for testing the api module
const fetchMockItemsCount = 10
const fetchMockItems = Array(fetchMockItemsCount).fill(null).map((_, i) => (
  { id: i }
))
global.fetchDelay = 0
global.fetchStatus = 200
global.fetchStatusText = 'OK'
global.fetch = jest.fn(() => new Promise((resolve) => {
  setTimeout(() => {
    resolve({
      status: global.fetchStatus,
      statusText: global.fetchStatusText,
      json: () => Promise.resolve(fetchMockItems)
    })
  }, global.fetchDelay)
}))

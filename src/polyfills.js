// A polyfilled environment for React 16
// https://reactjs.org/docs/javascript-environment-requirements.html
import 'core-js/es6/map'
import 'core-js/es6/set'
import 'raf/polyfill'

// code base polyfills
import 'core-js/fn/string/includes'
import 'core-js/es6/promise'
import 'whatwg-fetch'

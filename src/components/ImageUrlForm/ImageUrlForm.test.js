import React from 'react'
import { mount } from 'enzyme'
import ImageUrlForm, { GRAVITY_OPTIONS } from './ImageUrlForm'

const minWidth = 100
const maxWidth = 600
const defaultWidth = 200
const minHeight = 100
const maxHeight = 600
const defaultHeight = 200
const imageId = 1

describe('<ImageUrlForm />', () => {
  it('properly handles the width input', () => {
    const onChange = jest.fn()
    const wrapper = mount(
      <ImageUrlForm
        minWidth={minWidth}
        maxWidth={maxWidth}
        defaultWidth={defaultWidth}
        minHeight={minHeight}
        maxHeight={maxHeight}
        defaultHeight={defaultHeight}
        onChange={onChange}
      />
    )
    const input = wrapper.find('#width').hostNodes()
    input.simulate('change', {
      target: { value: `${minWidth + 10}` }
    })
    expect(onChange).toHaveBeenCalledTimes(1)
    expect(onChange.mock.calls[0][0]).toMatch(`${minWidth + 10}`)
    input.simulate('change', {
      target: { value: `${minWidth - 10}` }
    })
    expect(onChange).toHaveBeenCalledTimes(2)
    expect(onChange.mock.calls[1][0]).toMatch(`${minWidth}`)
    input.simulate('change', {
      target: { value: `${maxWidth + 10}` }
    })
    expect(onChange).toHaveBeenCalledTimes(3)
    expect(onChange.mock.calls[2][0]).toMatch(`${maxWidth}`)
    wrapper.unmount()
  })

  it('properly handles the height input', () => {
    const onChange = jest.fn()
    const wrapper = mount(
      <ImageUrlForm
        minWidth={minWidth}
        maxWidth={maxWidth}
        defaultWidth={defaultWidth}
        minHeight={minHeight}
        maxHeight={maxHeight}
        defaultHeight={defaultHeight}
        onChange={onChange}
      />
    )
    const input = wrapper.find('#height').hostNodes()
    input.simulate('change', {
      target: { value: `${minHeight + 10}` }
    })
    expect(onChange).toHaveBeenCalledTimes(1)
    expect(onChange.mock.calls[0][0]).toMatch(`${minHeight + 10}`)
    input.simulate('change', {
      target: { value: `${minHeight - 10}` }
    })
    expect(onChange).toHaveBeenCalledTimes(2)
    expect(onChange.mock.calls[1][0]).toMatch(`${minHeight}`)
    input.simulate('change', {
      target: { value: `${maxHeight + 10}` }
    })
    expect(onChange).toHaveBeenCalledTimes(3)
    expect(onChange.mock.calls[2][0]).toMatch(`${maxHeight}`)
    wrapper.unmount()
  })

  it('properly handles the grayscale input', () => {
    const onChange = jest.fn()
    const wrapper = mount(
      <ImageUrlForm
        minWidth={minWidth}
        maxWidth={maxWidth}
        defaultWidth={defaultWidth}
        minHeight={minHeight}
        maxHeight={maxHeight}
        defaultHeight={defaultHeight}
        onChange={onChange}
      />
    )
    const input = wrapper.find('#grayscale').hostNodes()
    input.simulate('change', {
      target: { checked: true }
    })
    expect(onChange).toHaveBeenCalledTimes(1)
    expect(onChange.mock.calls[0][0]).toMatch('/g/')
    wrapper.unmount()
  })

  it('properly handles the blur input', () => {
    const onChange = jest.fn()
    const wrapper = mount(
      <ImageUrlForm
        minWidth={minWidth}
        maxWidth={maxWidth}
        defaultWidth={defaultWidth}
        minHeight={minHeight}
        maxHeight={maxHeight}
        defaultHeight={defaultHeight}
        onChange={onChange}
      />
    )
    const input = wrapper.find('#blur').hostNodes()
    input.simulate('change', {
      target: { checked: true }
    })
    expect(onChange).toHaveBeenCalledTimes(1)
    expect(onChange.mock.calls[0][0]).toMatch('blur')
    wrapper.unmount()
  })

  it('properly handles the gravity input', () => {
    const onChange = jest.fn()
    const wrapper = mount(
      <ImageUrlForm
        minWidth={minWidth}
        maxWidth={maxWidth}
        defaultWidth={defaultWidth}
        minHeight={minHeight}
        maxHeight={maxHeight}
        defaultHeight={defaultHeight}
        onChange={onChange}
      />
    )
    const input = wrapper.find('#gravity').hostNodes()
    input.simulate('change', {
      target: { value: GRAVITY_OPTIONS[1].value }
    })
    expect(onChange).toHaveBeenCalledTimes(1)
    expect(onChange.mock.calls[0][0]).toMatch(GRAVITY_OPTIONS[1].value)
    wrapper.unmount()
  })

  it('can build urls to specific images', () => {
    const onChange = jest.fn()
    const wrapper = mount(
      <ImageUrlForm
        imageId={imageId}
        minWidth={minWidth}
        maxWidth={maxWidth}
        defaultWidth={defaultWidth}
        minHeight={minHeight}
        maxHeight={maxHeight}
        defaultHeight={defaultHeight}
        onChange={onChange}
      />
    )
    const input = wrapper.find('#width').hostNodes()
    input.simulate('change', {
      target: { value: `${minWidth + 10}` }
    })
    expect(onChange).toHaveBeenCalledTimes(1)
    expect(onChange.mock.calls[0][0]).toMatch(`?image=${imageId}`)
    wrapper.unmount()
  })
})

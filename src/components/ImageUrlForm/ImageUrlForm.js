import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { FormGroup, Switch, HTMLSelect } from '@blueprintjs/core'
import {
  buildUrl,
  GRAVITY_OPTIONS as LOREM_PICSUM_GRAVITY_OPTIONS
} from '../../api'
import NumberInput from '../NumberInput/NumberInput'
import { capitalize } from './utils'

export const GRAVITY_OPTIONS = ['none', ...LOREM_PICSUM_GRAVITY_OPTIONS].map(
  (opt) => ({
    value: opt !== 'none' ? opt : '',
    label: capitalize(opt)
  })
)

/**
 * @typedef {Object} ImageUrlFormProps
 * @property {number} minWidth
 * @property {number} maxWidth
 * @property {number} minHeight
 * @property {number} maxHeight
 * @property {number} defaultWidth
 * @property {number} defaultHeight
 * @property {number} [imageId]
 * @property {function(string)} [onChange]
 */

/**
 * @augments {Component<ImageUrlFormProps>}
 */
export default class ImageUrlForm extends Component {
  state = {
    width: this.props.defaultWidth,
    height: this.props.defaultHeight,
    grayscale: false,
    blur: false,
    gravity: GRAVITY_OPTIONS[0].value
  }

  render () {
    return (
      <div className="ImageUrlForm">
        <FormGroup
          label="Width"
          labelFor="width"
        >
          <NumberInput
            id="width"
            min={this.props.minWidth}
            max={this.props.maxWidth}
            value={this.state.width}
            onChange={(value) => this.handleChange(value, 'width')}
          />
        </FormGroup>
        <FormGroup
          label="Height"
          labelFor="height"
        >
          <NumberInput
            id="height"
            min={this.props.minHeight}
            max={this.props.maxHeight}
            value={this.state.height}
            onChange={(value) => this.handleChange(value, 'height')}
          />
        </FormGroup>
        <Switch
          id="grayscale"
          label="Grayscale"
          checked={this.state.grayscale}
          onChange={(e) => this.handleChange(e.target.checked, 'grayscale')}
        />
        <Switch
          id="blur"
          label="Blur"
          checked={this.state.blur}
          onChange={(e) => this.handleChange(e.target.checked, 'blur')}
        />
        <FormGroup
          label="Gravity"
          labelFor="gravity"
        >
          <HTMLSelect
            id="gravity"
            options={GRAVITY_OPTIONS}
            fill={true}
            value={this.state.gravity}
            onChange={(e) => this.handleChange(e.target.value, 'gravity')}
          />
        </FormGroup>
      </div>
    )
  }

  handleChange (value, name) {
    const { onChange } = this.props
    this.setState({ [name]: value }, () => {
      if (onChange) {
        onChange(this.buildUrl())
      }
    })
  }

  buildUrl () {
    let { maxWidth, minWidth, maxHeight, minHeight, imageId } = this.props
    let { width, height } = this.state
    if (width > maxWidth) { width = maxWidth }
    if (width < minWidth) { width = minWidth }
    if (height > maxHeight) { height = maxHeight }
    if (height < minHeight) { height = minHeight }
    return buildUrl({ ...this.state, width, height, id: imageId })
  }
}

ImageUrlForm.propTypes = {
  minWidth: PropTypes.number.isRequired,
  maxWidth: PropTypes.number.isRequired,
  minHeight: PropTypes.number.isRequired,
  maxHeight: PropTypes.number.isRequired,
  defaultWidth: PropTypes.number.isRequired,
  defaultHeight: PropTypes.number.isRequired,
  imageId: PropTypes.number,
  onChange: PropTypes.func
}

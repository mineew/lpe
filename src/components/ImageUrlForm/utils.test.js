import { capitalize } from './utils'

describe('capitalize', () => {
  it('capitalizes a string', () => {
    expect(capitalize('text')).toBe('Text')
    expect(capitalize('123')).toBe('123')
    expect(capitalize('')).toBe('')
    expect(capitalize(' text')).toBe(' text')
  })
})

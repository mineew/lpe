import React from 'react'
import { storiesOf } from '@storybook/react'
import { action } from '@storybook/addon-actions'
import { number } from '@storybook/addon-knobs'
import { Callout } from '@blueprintjs/core'
import ImageUrlForm from './ImageUrlForm'

const stories = storiesOf('Components', module)

const defaultContainerWidth = 400

const defaultMinWidth = 100
const defaultMaxWidth = 600
const defaultMinHeight = 100
const defaultMaxHeight = 600
const defaultDefaultWidth = 250
const defaultDefaultHeight = 200

stories.add('ImageUrlForm', () => {
  const minWidthProp = number('minWidth', defaultMinWidth)
  const maxWidthProp = number('maxWidth', defaultMaxWidth)
  const defaultWidthProp = number('defaultWidth', defaultDefaultWidth)
  const minHeightProp = number('minHeight', defaultMinHeight)
  const maxHeightProp = number('maxHeight', defaultMaxHeight)
  const defaultHeightProp = number('defaultHeight', defaultDefaultHeight)
  const containerWidth = number('[Container Width]', defaultContainerWidth)
  if (!_sizesAreOk({
    minWidth: minWidthProp,
    maxWidth: maxWidthProp,
    defaultWidth: defaultWidthProp,
    minHeight: minHeightProp,
    maxHeight: maxHeightProp,
    defaultHeight: defaultHeightProp
  })) {
    return (
      <Callout intent="danger">
        The sizes of an image are incorrect.
      </Callout>
    )
  }
  return (
    <div style={{ width: containerWidth }}>
      <ImageUrlForm
        imageId={1}
        minWidth={minWidthProp}
        maxWidth={maxWidthProp}
        defaultWidth={defaultWidthProp}
        minHeight={minHeightProp}
        maxHeight={maxHeightProp}
        defaultHeight={defaultHeightProp}
        onChange={action('onChange')}
      />
    </div>
  )
})

/// ----------------------------------------------------------------------------

function _sizesAreOk (sizes) {
  return Number.isInteger(sizes.minWidth) && sizes.minWidth > 0 &&
         Number.isInteger(sizes.maxWidth) && sizes.maxWidth > 0 &&
         Number.isInteger(sizes.defaultWidth) && sizes.defaultWidth > 0 &&
         Number.isInteger(sizes.minHeight) && sizes.minHeight > 0 &&
         Number.isInteger(sizes.maxHeight) && sizes.maxHeight > 0 &&
         Number.isInteger(sizes.defaultHeight) && sizes.defaultHeight > 0
}

/**
 * @param {string} text
 * @returns {string}
 */
export function capitalize (text) {
  return text.charAt(0).toUpperCase() + text.slice(1)
}

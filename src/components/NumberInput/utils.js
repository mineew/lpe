/**
 * @param {string} text
 * @returns {number}
 */
export function numbersOnly (text) {
  const numbers = '0123456789'
  let filtered = text.split('').filter(
    (char) => numbers.includes(char)
  )
  return Number(filtered.join(''))
}

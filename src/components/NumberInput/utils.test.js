import { numbersOnly } from './utils'

describe('numbersOnly', () => {
  it('leaves only numbers in the string', () => {
    expect(numbersOnly('  123.10 asdf ')).toBe(12310)
    expect(numbersOnly('///...sdf!!')).toBe(0)
    expect(numbersOnly('')).toBe(0)
  })
})

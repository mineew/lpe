import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { InputGroup, ButtonGroup, Button } from '@blueprintjs/core'
import { KEY_UP, KEY_DOWN } from 'keycode-js'
import { numbersOnly } from './utils'

/**
 * @typedef {Object} NumberInputProps
 * @property {number} min
 * @property {number} max
 * @property {number} value
 * @property {function(number)} onChange
 * @property {string} [id]
 */

/**
 * @augments {Component<NumberInputProps>}
 */
export default class NumberInput extends Component {
  render () {
    const { id, value } = this.props
    return (
      <InputGroup
        className="NumberInput"
        id={id}
        rightElement={this.renderToolbar()}
        value={value}
        onChange={this.handleChange}
        onBlur={this.handleBlur}
        onKeyDown={this.handleKeyDown}
      />
    )
  }

  renderToolbar () {
    const { onChange, value, min, max } = this.props
    return (
      <ButtonGroup minimal={true}>
        <Button
          icon="plus"
          disabled={value === max}
          onClick={() => onChange(this.constrain(value + 1))}
        />
        <Button
          icon="minus"
          disabled={value === min}
          onClick={() => onChange(this.constrain(value - 1))}
        />
        <Button
          icon="maximize"
          disabled={value === max}
          onClick={() => onChange(max)}
        />
        <Button
          icon="minimize"
          disabled={value === min}
          onClick={() => onChange(min)}
        />
      </ButtonGroup>
    )
  }

  /**
   * @param {Event} e
   */
  handleChange = (e) => {
    const { onChange } = this.props
    onChange(numbersOnly(e.target.value))
  }

  /**
   * @param {Event} e
   */
  handleBlur = (e) => {
    const { onChange } = this.props
    onChange(this.constrain(Number(e.target.value)))
  }

  /**
   * @param {React.KeyboardEvent} e
   */
  handleKeyDown = (e) => {
    const { onChange, value } = this.props
    if (e.keyCode === KEY_UP) {
      onChange(this.constrain(value + 1))
    }
    if (e.keyCode === KEY_DOWN) {
      onChange(this.constrain(value - 1))
    }
  }

  constrain (value) {
    const { min, max } = this.props
    if (value < min) { return min }
    if (value > max) { return max }
    return value
  }
}

NumberInput.propTypes = {
  min: PropTypes.number.isRequired,
  max: PropTypes.number.isRequired,
  value: PropTypes.number.isRequired,
  onChange: PropTypes.func.isRequired,
  id: PropTypes.string
}

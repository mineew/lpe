import React, { Component, Fragment } from 'react'
import { storiesOf } from '@storybook/react'
import { action } from '@storybook/addon-actions'
import { text, number } from '@storybook/addon-knobs'
import { Callout, Code, Pre, Tag, Classes } from '@blueprintjs/core'
import NumberInput from './NumberInput'

const stories = storiesOf('Components', module)

const defaultId = ''
const defaultMin = 10
const defaultMax = 100
const defaultValue = 55

stories.add('NumberInput', () => {
  const idProp = text('id', defaultId)
  const minProp = number('min', defaultMin)
  const maxProp = number('max', defaultMax)
  const valueProp = number('value', defaultValue)
  if (!_minMaxAreOk(minProp, maxProp)) {
    return (
      <Callout intent="danger">
        The <Code>min</Code> and <Code>max</Code> props are incorrect.
      </Callout>
    )
  }
  return (
    <Fragment>
      <NumberInput
        key={valueProp}
        id={idProp}
        min={minProp}
        max={maxProp}
        value={valueProp}
        onChange={action('onChange')}
      />
      <Callout
        title="Within a stateful parent"
        icon="pin"
        style={{ marginTop: 20 }}
      >
        <Stateful />
      </Callout>
    </Fragment>
  )
})

/// ----------------------------------------------------------------------------

function _minMaxAreOk (min, max) {
  return Number.isInteger(min) &&
         Number.isInteger(max) &&
         min < max
}

class Stateful extends Component {
  state = {
    value: 15,
    changes: []
  }

  render () {
    return (
      <Fragment>
        <NumberInput
          min={5}
          max={50}
          value={this.state.value}
          onChange={(value) => {
            this.setState((prevState) => {
              return { value, changes: [value].concat(prevState.changes) }
            })
          }}
        />
        <Pre>
          {'<NumberInput\n  min={5}\n  max={50}\n  value={this.state.value}\n'}
          {'  onChange={(value) => this.setState({ value })}\n/>'}
        </Pre>
        <div className={Classes.TEXT_OVERFLOW_ELLIPSIS}>
          {this.state.changes.map((value, i) => (
            <Tag key={i} intent="warning" style={{ marginRight: 5 }}>
              {value}
            </Tag>
          ))}
        </div>
      </Fragment>
    )
  }
}

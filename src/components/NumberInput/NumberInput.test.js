import React from 'react'
import { mount } from 'enzyme'
import { KEY_UP, KEY_DOWN } from 'keycode-js'
import NumberInput from './NumberInput'

const min = 10
const max = 100
const value = 55

describe('<NumberInput />', () => {
  it('increments', () => {
    const onChange = jest.fn()
    const wrapper = mount(
      <NumberInput
        min={min}
        max={max}
        value={value}
        onChange={onChange}
      />
    )
    wrapper.find('button').at(0).simulate('click')
    expect(onChange).toHaveBeenCalledTimes(1)
    expect(onChange).toHaveBeenLastCalledWith(value + 1)
    wrapper.find('input').simulate('keydown', { keyCode: KEY_UP })
    expect(onChange).toHaveBeenCalledTimes(2)
    expect(onChange).toHaveBeenLastCalledWith(value + 1)
    wrapper.find('button').at(1).simulate('click')
    expect(onChange).toHaveBeenCalledTimes(3)
    expect(onChange).toHaveBeenLastCalledWith(value - 1)
    wrapper.find('input').simulate('keydown', { keyCode: KEY_DOWN })
    expect(onChange).toHaveBeenCalledTimes(4)
    expect(onChange).toHaveBeenLastCalledWith(value - 1)
    wrapper.unmount()
  })

  it('minimizes and maximizes', () => {
    const onChange = jest.fn()
    const wrapper = mount(
      <NumberInput
        min={min}
        max={max}
        value={value}
        onChange={onChange}
      />
    )
    wrapper.find('button').at(2).simulate('click')
    expect(onChange).toHaveBeenCalledTimes(1)
    expect(onChange).toHaveBeenLastCalledWith(max)
    wrapper.find('button').at(3).simulate('click')
    expect(onChange).toHaveBeenCalledTimes(2)
    expect(onChange).toHaveBeenLastCalledWith(min)
    wrapper.unmount()
  })

  it('accepts only numbers', () => {
    const onChange = jest.fn()
    const wrapper = mount(
      <NumberInput
        min={min}
        max={max}
        value={value}
        onChange={onChange}
      />
    )
    wrapper.find('input').simulate('change', { target: { value: 'kwm123.1' } })
    expect(onChange).toHaveBeenCalledTimes(1)
    expect(onChange).toHaveBeenLastCalledWith(1231)
    wrapper.unmount()
  })

  it('constrains on blur', () => {
    const onChange = jest.fn()
    let wrapper = mount(
      <NumberInput
        min={min}
        max={max}
        value={max + 1}
        onChange={onChange}
      />
    )
    wrapper.find('input').simulate('blur')
    expect(onChange).toHaveBeenCalledTimes(1)
    expect(onChange).toHaveBeenLastCalledWith(max)
    wrapper.unmount()
    wrapper = mount(
      <NumberInput
        min={min}
        max={max}
        value={min - 1}
        onChange={onChange}
      />
    )
    wrapper.find('input').simulate('blur')
    expect(onChange).toHaveBeenCalledTimes(2)
    expect(onChange).toHaveBeenLastCalledWith(min)
    wrapper.unmount()
  })
})

import React, { Component } from 'react'
import PropTypes from 'prop-types'
import Loader from '../Loader/Loader'
import './Image.css'

/**
 * @typedef {Object} ImageProps
 * @property {string} src
 * @property {number} hostWidth
 * @property {number} hostHeight
 * @property {string} [alt]
 * @property {function} [onLoaded]
 */

/**
 * @augments {Component<ImageProps>}
 */
export default class Image extends Component {
  state = {
    loading: true,
    error: false
  }

  render () {
    const { src, alt, hostWidth: width, hostHeight: height } = this.props
    const { loading, error } = this.state
    const display = loading || error ? 'none' : null
    return (
      <div className="Image" style={{ width, height }}>
        {(error || loading) && (
          <Loader
            loading={loading}
            error={error}
            message={error ? 'This image can\'t be loaded' : ''}
          />
        )}
        <img
          src={src}
          alt={alt || src}
          onLoad={this.handleLoad}
          onError={this.handleError}
          style={{ display, maxWidth: width, maxHeight: height }}
        />
      </div>
    )
  }

  handleLoad = () => {
    this.setState({ loading: false })
    if (this.props.onLoaded) {
      this.props.onLoaded()
    }
  }

  handleError = () => {
    this.setState({ loading: false, error: true })
  }
}

Image.propTypes = {
  src: PropTypes.string.isRequired,
  hostWidth: PropTypes.number.isRequired,
  hostHeight: PropTypes.number.isRequired,
  alt: PropTypes.string,
  onLoaded: PropTypes.func
}

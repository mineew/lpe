import React from 'react'
import { shallow } from 'enzyme'
import Image from './Image'

const src = 'https://picsum.photos/300/250?image=1'
const width = 400
const height = 350

describe('<Image />', () => {
  it('hides image tag while loading or if there is an error', () => {
    const wrapper = shallow(
      <Image src={src} hostWidth={width} hostHeight={height} />
    )
    expect(wrapper.find('img').html()).toMatch('display:none')
    wrapper.find('img').simulate('load')
    expect(wrapper.find('img').html().includes('display:none')).toBeFalsy()
    wrapper.find('img').simulate('error')
    expect(wrapper.find('img').html()).toMatch('display:none')
  })

  it('invokes callback when it finishes loading', () => {
    const onLoaded = jest.fn()
    const wrapper = shallow(
      <Image
        src={src}
        hostWidth={width}
        hostHeight={height}
        onLoaded={onLoaded}
      />
    )
    wrapper.find('img').simulate('load')
    expect(onLoaded).toHaveBeenCalled()
  })
})

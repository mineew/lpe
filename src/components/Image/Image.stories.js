import React from 'react'
import { storiesOf } from '@storybook/react'
import { action } from '@storybook/addon-actions'
import { number, text } from '@storybook/addon-knobs'
import Image from './Image'

const stories = storiesOf('Components', module)

const defaultSrc = 'https://picsum.photos/300/250?image=1'
const defaultAlt = ''
const defaultHostWidth = 400
const defaultHostHeight = 350

stories.add('Image', () => {
  const srcProp = text('src', defaultSrc)
  const altProp = text('alt', defaultAlt)
  const hostWidthProp = number('hostWidth', defaultHostWidth)
  const hostHeightProp = number('hostHeight', defaultHostHeight)
  return (
    <Image
      key={srcProp}
      src={srcProp}
      alt={altProp}
      hostWidth={hostWidthProp}
      hostHeight={hostHeightProp}
      onLoaded={action('onLoaded')}
    />
  )
})

import React from 'react'
import { storiesOf } from '@storybook/react'
import { action } from '@storybook/addon-actions'
import { object, number } from '@storybook/addon-knobs'
import { Callout, Code } from '@blueprintjs/core'
import ImageList from './ImageList'

const stories = storiesOf('Components', module)

const defaultImageWidth = 125
const defaultImageHeight = 100
const defaultImagesCount = 5
const defaultImages = Array(defaultImagesCount).fill(null).map((_, i) => ({
  id: i + 1,
  src: _getUrl(i + 1, defaultImageWidth, defaultImageHeight)
}))

stories.add('ImageList', () => {
  const imagesProp = object('images', defaultImages)
  const imageWidthProp = number('imageWidth', defaultImageWidth)
  const imageHeightProp = number('imageHeight', defaultImageHeight)
  if (!_imagesAreOk(imagesProp)) {
    return (
      <Callout intent="danger">
        The <Code>images</Code> prop is incorrect.
      </Callout>
    )
  }
  return (
    <ImageList
      images={imagesProp}
      imageWidth={imageWidthProp}
      imageHeight={imageHeightProp}
      onSelect={action('onSelect')}
    />
  )
})

/// ----------------------------------------------------------------------------

function _getUrl (id, width, height) {
  return `https://picsum.photos/${width}/${height}/?image=${id}`
}

function _imagesAreOk (images) {
  for (let img of images) {
    const { id, src } = img
    const idIsOk = Number.isInteger(id)
    const srcIsOk = typeof src === 'string'
    if (!idIsOk || !srcIsOk) {
      return false
    }
  }
  return true
}

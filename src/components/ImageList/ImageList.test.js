import React from 'react'
import { shallow } from 'enzyme'
import Image from '../Image/Image'
import ImageListItem from '../ImageListItem/ImageListItem'
import ImageList from './ImageList'

const imageWidth = 125
const imageHeight = 100
const imagesCount = 5
const images = Array(imagesCount).fill(null).map((_, i) => ({
  id: i + 1,
  src: `https://picsum.photos/${imageWidth}/${imageHeight}/?image=${i + 1}`
}))

describe('<ImageList />', () => {
  it('can select images from the list', () => {
    const onSelect = jest.fn()
    const wrapper = shallow(
      <ImageList
        images={images}
        imageWidth={imageWidth}
        imageHeight={imageHeight}
        onSelect={onSelect}
      />
    )
    for (let i = 0; i < images.length; i++) {
      const imageListItem = wrapper.find(ImageListItem).at(i).dive()
      const image = imageListItem.find(Image).dive()
      image.find('img').simulate('load')
      imageListItem.simulate('click')
      expect(onSelect).toHaveBeenLastCalledWith(images[i].id)
    }
    expect(onSelect).toHaveBeenCalledTimes(images.length)
  })
})

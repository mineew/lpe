import React from 'react'
import PropTypes from 'prop-types'
import ImageListItem from '../ImageListItem/ImageListItem'
import './ImageList.css'

/**
 * @typedef {Object} ImageListItemInfo
 * @property {number} id
 * @property {string} src
 * @property {string} [alt]
 */

/**
 * @typedef {Object} ImageListProps
 * @property {Array.<ImageListItemInfo>} images
 * @property {number} imageWidth
 * @property {number} imageHeight
 * @property {function(number)} [onSelect]
 */

/**
 * @param {ImageListProps} props
 * @returns {React.Component<ImageListProps>}
 */
export default function ImageList (props) {
  return (
    <div className="ImageList">
      {props.images.map((image) => (
        <ImageListItem
          key={`${image.src}-${image.id}`}
          id={image.id}
          src={image.src}
          alt={image.alt}
          width={props.imageWidth}
          height={props.imageHeight}
          onClick={props.onSelect}
        />
      ))}
    </div>
  )
}

const imageListItemInfo = PropTypes.shape({
  id: PropTypes.number.isRequired,
  src: PropTypes.string.isRequired,
  alt: PropTypes.string
})

ImageList.propTypes = {
  images: PropTypes.arrayOf(imageListItemInfo),
  imageWidth: PropTypes.number.isRequired,
  imageHeight: PropTypes.number.isRequired,
  onSelect: PropTypes.func
}

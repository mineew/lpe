import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { Card, Elevation } from '@blueprintjs/core'
import Image from '../Image/Image'
import './ImageListItem.css'

/**
 * @typedef {Object} ImageListItemProps
 * @property {number} id
 * @property {string} src
 * @property {number} width
 * @property {number} height
 * @property {string} [alt]
 * @property {function(number)} [onClick]
 */

/**
 * @augments {Component<ImageListItemProps>}
 */
export default class ImageListItem extends Component {
  state = {
    loaded: false
  }

  render () {
    const { src, alt, width, height } = this.props
    const { loaded } = this.state
    return (
      <Card
        className="ImageListItem"
        style={{ width, height }}
        elevation={loaded ? Elevation.ONE : Elevation.ZERO}
        interactive={loaded}
        onClick={this.handleClick}
      >
        <Image
          src={src}
          alt={alt}
          hostWidth={width}
          hostHeight={height}
          onLoaded={this.handleImageLoad}
        />
      </Card>
    )
  }

  handleClick = () => {
    const { id, onClick } = this.props
    const { loaded } = this.state
    if (loaded && onClick) {
      onClick(id)
    }
  }

  handleImageLoad = () => {
    this.setState({ loaded: true })
  }
}

ImageListItem.propTypes = {
  id: PropTypes.number.isRequired,
  src: PropTypes.string.isRequired,
  width: PropTypes.number.isRequired,
  height: PropTypes.number.isRequired,
  alt: PropTypes.string,
  onClick: PropTypes.func
}

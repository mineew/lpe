import React from 'react'
import { storiesOf } from '@storybook/react'
import { action } from '@storybook/addon-actions'
import { number, text } from '@storybook/addon-knobs'
import ImageListItem from './ImageListItem'

const stories = storiesOf('Components', module)

const defaultSrc = 'https://picsum.photos/150/100?image=1'
const defaultAlt = ''
const defaultId = 1
const defaultWidth = 150
const defaultHeight = 100

stories.add('ImageListItem', () => {
  const srcProp = text('src', defaultSrc)
  const altProp = text('alt', defaultAlt)
  const idProp = number('id', defaultId)
  const widthProp = number('width', defaultWidth)
  const heightProp = number('height', defaultHeight)
  return (
    <ImageListItem
      key={srcProp}
      src={srcProp}
      alt={altProp}
      id={idProp}
      width={widthProp}
      height={heightProp}
      onClick={action('onClick')}
    />
  )
})

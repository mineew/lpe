import React from 'react'
import { shallow } from 'enzyme'
import Image from '../Image/Image'
import ImageListItem from './ImageListItem'

const id = 1
const src = 'https://picsum.photos/300/250?image=1'
const width = 400
const height = 350

describe('<ImageListItem />', () => {
  it('handle clicks', () => {
    const onClick = jest.fn()
    const wrapper = shallow(
      <ImageListItem
        id={id}
        src={src}
        width={width}
        height={height}
        onClick={onClick}
      />
    )
    wrapper.find(Image).dive().find('img').simulate('load')
    wrapper.simulate('click')
    expect(onClick).toHaveBeenCalledTimes(1)
    expect(onClick).toHaveBeenLastCalledWith(id)
  })

  it('doesn\'t handle clicks while loading or if there is an error', () => {
    const onClick = jest.fn()
    const wrapper = shallow(
      <ImageListItem
        id={id}
        src={src}
        width={width}
        height={height}
        onClick={onClick}
      />
    )
    wrapper.simulate('click')
    wrapper.find(Image).dive().find('img').simulate('error')
    wrapper.simulate('click')
    expect(onClick).toHaveBeenCalledTimes(0)
  })
})

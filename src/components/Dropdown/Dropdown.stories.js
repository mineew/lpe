import React from 'react'
import { storiesOf } from '@storybook/react'
import { action } from '@storybook/addon-actions'
import { object, number } from '@storybook/addon-knobs'
import { Callout, Code } from '@blueprintjs/core'
import Dropdown from './Dropdown'

const stories = storiesOf('Components', module)

const defaultOptionsCount = 5
const defaultOptions = Array(defaultOptionsCount).fill(null).map((_, i) => ({
  text: `option ${i + 1}`,
  value: i + 1
}))
const defaultValue = 2

stories.add('Dropdown', () => {
  const optionsProp = object('options', defaultOptions)
  const valueProp = number('value', defaultValue)
  if (!_optionsAreOk(optionsProp)) {
    return (
      <Callout intent="danger">
        The <Code>options</Code> prop is incorrect.
      </Callout>
    )
  }
  if (!_valueIsOk(valueProp, optionsProp)) {
    return (
      <Callout intent="danger">
        The <Code>value</Code> prop is out of range.
      </Callout>
    )
  }
  return (
    <Dropdown
      options={optionsProp}
      value={valueProp}
      onChange={action('onChange')}
    />
  )
})

/// ----------------------------------------------------------------------------

function _getMin (options) {
  const values = options.map(({ value }) => value)
  return Math.min(...values)
}

function _getMax (options) {
  const values = options.map(({ value }) => value)
  return Math.max(...values)
}

function _optionsAreOk (options) {
  const values = []
  for (let opt of options) {
    const { value, text } = opt
    const valueIsOk = Number.isInteger(value) && !values.includes(value)
    const textIsOk = text && typeof text === 'string'
    if (!valueIsOk || !textIsOk) {
      return false
    }
    values.push(value)
  }
  return true
}

function _valueIsOk (value, options) {
  return Number.isInteger(value) &&
         value >= _getMin(options) &&
         value <= _getMax(options)
}

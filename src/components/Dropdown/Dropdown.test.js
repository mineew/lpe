import React from 'react'
import { shallow, mount } from 'enzyme'
import Dropdown from './Dropdown'

const options = [{
  text: 'option 1',
  value: 1
}, {
  text: 'option 2',
  value: 2
}, {
  text: 'option 3',
  value: 3
}]

const value = 2

describe('<Dropdown />', () => {
  it('throws when value doesn\'t correspond to any of options', () => {
    expect.assertions(1)
    try {
      shallow(<Dropdown options={options} value={7} />)
    } catch (e) {
      expect(e).toBeDefined()
    }
  })

  it('renders options', () => {
    const wrapper = mount(<Dropdown options={options} value={value} />)
    wrapper.find('button').simulate('click')
    const lis = wrapper.find('li')
    for (let i = 0; i < options.length; i++) {
      expect(lis.at(i).text()).toBe(options[i].text)
    }
    wrapper.unmount()
  })

  it('changes value', () => {
    const onChange = jest.fn()
    const wrapper = mount(
      <Dropdown
        options={options}
        value={value}
        onChange={onChange}
      />
    )
    wrapper.find('button').simulate('click')
    const links = wrapper.find('a')
    for (let i = 0; i < options.length; i++) {
      links.at(i).simulate('click')
      expect(onChange).toHaveBeenLastCalledWith(options[i].value)
    }
    expect(onChange).toHaveBeenCalledTimes(options.length)
    wrapper.unmount()
  })
})

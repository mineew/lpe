import React from 'react'
import PropTypes from 'prop-types'
import { Popover, Menu, Button, Position } from '@blueprintjs/core'

/**
 * @typedef {Object} DropdownOption
 * @property {string} text
 * @property {number} value
 */

/**
 * @typedef {Object} DropdownProps
 * @property {Array.<DropdownOption>} options
 * @property {number} value
 * @property {function(number)} [onChange]
 */

/**
 * @param {DropdownProps} props
 * @returns {React.Component<DropdownProps>}
 */
export default function Dropdown (props) {
  const selectedOptions = props.options.filter(
    (opt) => opt.value === props.value
  )
  if (selectedOptions.length !== 1) {
    throw new Error('Can\'t find selected option by value.')
  }
  const selected = selectedOptions[0]
  return (
    <Popover
      content={_dropdownMenu(props.options, selected, props.onChange)}
      position={Position.BOTTOM}
      usePortal={false}
    >
      <Button
        className="Dropdown"
        rightIcon="caret-down"
        text={selected.text}
      />
    </Popover>
  )
}

const dropdownOption = PropTypes.shape({
  text: PropTypes.string.isRequired,
  value: PropTypes.number.isRequired
})

Dropdown.propTypes = {
  options: PropTypes.arrayOf(dropdownOption).isRequired,
  value: PropTypes.number.isRequired,
  onChange: PropTypes.func
}

/// ----------------------------------------------------------------------------

/**
 * @private
 * @param {Array.<DropdownOption>} options
 * @param {DropdownOption} selected
 * @returns {React.ReactNode}
 */
function _dropdownMenu (options, selected, onClick) {
  return (
    <Menu>
      {options.map((opt) => (
        <Menu.Item
          key={opt.value}
          text={opt.text}
          active={opt.value === selected.value}
          onClick={onClick && (() => onClick(opt.value))}
        />
      ))}
    </Menu>
  )
}

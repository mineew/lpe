import * as componentsModule from '.'

describe('components module', () => {
  it('exports desired set of components', () => {
    expect(componentsModule.Loader).toBeDefined()
    expect(componentsModule.Navbar).toBeDefined()
    expect(componentsModule.Pagination).toBeDefined()
    expect(componentsModule.ImageList).toBeDefined()
    expect(componentsModule.ImageDialog).toBeDefined()
  })
})

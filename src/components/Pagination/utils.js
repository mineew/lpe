/**
 * @param {number} itemsCount
 * @param {number} pageSize
 * @returns {number}
 */
export function getPageCount (itemsCount, pageSize) {
  let count = itemsCount / pageSize
  if (Math.floor(count) < count) {
    return Math.floor(count) + 1
  }
  return count
}

/**
 * @template T
 * @param {Array.<T>} items
 * @param {number} currentPage starts from 1
 * @param {number} pageSize
 * @returns {Array.<T>}
 */
export function getPageItems (items, currentPage, pageSize) {
  return items.slice(
    (currentPage - 1) * pageSize,
    (currentPage - 1) * pageSize + pageSize
  )
}

import React from 'react'
import PropTypes from 'prop-types'
import { Button } from '@blueprintjs/core'
import { getPaginationModel, ITEM_TYPES } from 'ultimate-pagination'
import Dropdown from '../Dropdown/Dropdown'
import { getPageCount, getPageItems } from './utils'
import './Pagination.css'

/**
 * @typedef {Object} PaginationProps
 * @property {Array.<*>} items
 * @property {number} pageSize
 * @property {number} page
 * @property {function(Array.<*>)} children
 * @property {function(number)} [onPageChange]
 * @property {Array.<number>} [pageSizeOptions]
 * @property {function(number, number)} [onPageSizeChange]
 */

/**
 * @param {PaginationProps} props
 * @returns {React.Component<PaginationProps>}
 */
export default function Pagination (props) {
  const {
    items, page, pageSize, onPageChange, pageSizeOptions, onPageSizeChange
  } = props
  const model = getPaginationModel({
    currentPage: page,
    totalPages: getPageCount(items.length, pageSize),
    hideFirstAndLastPageLinks: true
  })
  return (
    <div className="Pagination">
      <div className="Pagination-page">
        {props.children(getPageItems(items, page, pageSize))}
      </div>
      <div className="Pagination-controls">
        {model.map((btnOpts) => {
          switch (btnOpts.type) {
            case ITEM_TYPES.PREVIOUS_PAGE_LINK: {
              return _prevButton(btnOpts, onPageChange)
            }
            case ITEM_TYPES.NEXT_PAGE_LINK: {
              return _nextButton(btnOpts, onPageChange)
            }
            case ITEM_TYPES.PAGE: {
              return _pageButton(btnOpts, onPageChange)
            }
            case ITEM_TYPES.ELLIPSIS: {
              return _ellipsisButton(btnOpts, onPageChange)
            }
            /* istanbul ignore next */
            default: {
              return null
            }
          }
        })}
        {pageSizeOptions && (
          <Dropdown
            options={pageSizeOptions.map((value) => ({
              text: `${value} / page`,
              value
            }))}
            value={pageSize}
            onChange={onPageSizeChange && ((newPageSize) => {
              let newPageCount = getPageCount(items.length, newPageSize)
              let newPage = page > newPageCount ? newPageCount : page
              onPageSizeChange(newPageSize, newPage)
            })}
          />
        )}
      </div>
    </div>
  )
}

Pagination.propTypes = {
  items: PropTypes.array.isRequired,
  pageSize: PropTypes.number.isRequired,
  page: PropTypes.number.isRequired,
  children: PropTypes.func.isRequired,
  onPageChange: PropTypes.func,
  pageSizeOptions: PropTypes.arrayOf(PropTypes.number),
  onPageSizeChange: PropTypes.func
}

/// ----------------------------------------------------------------------------

/**
 * @typedef {Object} PaginationButtonOptions
 * @property {number} key
 * @property {number} value
 * @property {boolean} isActive
 */

/**
 * @private
 * @param {PaginationButtonOptions} opts
 * @param {function(number)} onClick
 * @param {string} [icon]
 * @param {boolean} [minimal]
 * @returns {React.ReactNode}
 */
function _button (opts, onClick, icon, minimal) {
  return (
    <Button
      key={opts.key}
      icon={icon}
      text={!icon && opts.value}
      minimal={minimal}
      disabled={opts.isActive}
      onClick={onClick && (() => onClick(opts.value))}
    />
  )
}

/**
 * @private
 * @param {PaginationButtonOptions} opts
 * @param {function(number)} onClick
 * @returns {React.ReactNode}
 */
function _prevButton (opts, onClick) {
  return _button(opts, onClick, 'chevron-left')
}

/**
 * @private
 * @param {PaginationButtonOptions} opts
 * @param {function(number)} onClick
 * @returns {React.ReactNode}
 */
function _nextButton (opts, onClick) {
  return _button(opts, onClick, 'chevron-right')
}

/**
 * @private
 * @param {PaginationButtonOptions} opts
 * @param {function(number)} onClick
 * @returns {React.ReactNode}
 */
function _pageButton (opts, onClick) {
  return _button(opts, onClick)
}

/**
 * @private
 * @param {PaginationButtonOptions} opts
 * @param {function(number)} onClick
 * @returns {React.ReactNode}
 */
function _ellipsisButton (opts, onClick) {
  return _button(opts, onClick, 'more', true)
}

import { getPageCount, getPageItems } from './utils'

describe('getPageCount', () => {
  it('returns count of pages', () => {
    let count = getPageCount(80, 20)
    expect(count).toBe(4)
  })
  it('can process division with remainder', () => {
    let count = getPageCount(57, 10)
    expect(count).toBe(6)
  })
})

describe('getPageItems', () => {
  it('returns a chunk of an array corresponds to the page number', () => {
    let array = [1, 2, 3, 4, 5, 6, 7, 8, 9]
    let page2 = getPageItems(array, 2, 3)
    expect(page2).toEqual([4, 5, 6])
  })
})

import React from 'react'
import { storiesOf } from '@storybook/react'
import { action } from '@storybook/addon-actions'
import { array, number } from '@storybook/addon-knobs'
import { UL, Callout, Code } from '@blueprintjs/core'
import Pagination from './Pagination'
import { getPageCount } from './utils'

const stories = storiesOf('Components', module)

const defaultItemsCount = 87
const defaultItems = Array(defaultItemsCount).fill(null).map(
  (_, i) => i + 1
)
const defaultPageSize = 7
const defaultPage = 1
const defaultPageSizeOptions = [7, 14, 21]

stories.add('Pagination', () => {
  const itemsProp = array('items', defaultItems)
  const pageSizeProp = number('pageSize', defaultPageSize)
  const pageProp = number('page', defaultPage)
  const pageSizeOptionsProp = array('pageSizeOptions', defaultPageSizeOptions)
  if (!_pageSizeOptionsAreOk(pageSizeOptionsProp)) {
    return (
      <Callout intent="danger">
        The <Code>pageSizeOptions</Code> prop is incorrect.
      </Callout>
    )
  }
  if (!_pageSizeIsOk(pageSizeProp, pageSizeOptionsProp)) {
    return (
      <Callout intent="danger">
        The <Code>pageSize</Code> prop must be one of
        {' '}[{pageSizeOptionsProp.join(', ')}].
      </Callout>
    )
  }
  if (!_pageIsOk(itemsProp, pageSizeProp, pageProp)) {
    return (
      <Callout intent="danger">
        The <Code>page</Code> prop is out of range.
      </Callout>
    )
  }
  return (
    <Pagination
      items={itemsProp}
      pageSize={pageSizeProp}
      page={pageProp}
      onPageChange={action('onPageChange')}
      pageSizeOptions={pageSizeOptionsProp.map((opt) => Number(opt))}
      onPageSizeChange={action('onPageSizeChange')}
    >
      {(pageItems) => (
        <UL>
          {pageItems.map((item) => (
            <li key={item}>{item}</li>
          ))}
        </UL>
      )}
    </Pagination>
  )
})

/// ----------------------------------------------------------------------------

function _pageSizeIsOk (pageSize, pageSizeOptions) {
  return pageSizeOptions.map((opt) => Number(opt)).includes(pageSize)
}

function _pageIsOk (items, pageSize, page) {
  return Number.isInteger(page) &&
         page > 0 &&
         page <= getPageCount(items.length, pageSize)
}

function _pageSizeOptionsAreOk (pageSizeOptions) {
  return pageSizeOptions.map((opt) => Number(opt)).every(
    (opt) => Number.isInteger(opt) && opt > 0
  )
}

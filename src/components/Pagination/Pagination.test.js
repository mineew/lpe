import React from 'react'
import { shallow, mount } from 'enzyme'
import Pagination from './Pagination'

const items = Array(30).fill(null).map((_, i) => i + 1)
const pageSizeOptions = [3, 5, 15]
const pageSize = 3
const page = 1
const lastPage = 9

describe('<Pagination />', () => {
  it('calls children', () => {
    const children = jest.fn()
    shallow(
      <Pagination
        items={items}
        pageSize={pageSize}
        page={page}
        pageSizeOptions={pageSizeOptions}
      >
        {children}
      </Pagination>
    )
    expect(children).toHaveBeenCalledTimes(1)
  })

  it('can change page', () => {
    const children = jest.fn()
    const onPageChange = jest.fn()
    const wrapper = mount(
      <Pagination
        items={items}
        pageSize={pageSize}
        page={page}
        pageSizeOptions={pageSizeOptions}
        onPageChange={onPageChange}
      >
        {children}
      </Pagination>
    )
    // disabled buttons
    wrapper.find('button').at(0).simulate('click')
    wrapper.find('button').at(1).simulate('click')
    expect(onPageChange).toHaveBeenCalledTimes(0)
    // regular buttons
    wrapper.find('button').at(2).simulate('click')
    expect(onPageChange).toHaveBeenCalledTimes(1)
    expect(onPageChange).toHaveBeenLastCalledWith(2)
    wrapper.find('button').at(3).simulate('click')
    expect(onPageChange).toHaveBeenCalledTimes(2)
    expect(onPageChange).toHaveBeenLastCalledWith(3)
    // ellipsis button
    wrapper.find('button').at(6).simulate('click')
    expect(onPageChange).toHaveBeenCalledTimes(3)
    expect(onPageChange).toHaveBeenLastCalledWith(6)
    // next button
    wrapper.find('button').at(8).simulate('click')
    expect(onPageChange).toHaveBeenCalledTimes(4)
    expect(onPageChange).toHaveBeenLastCalledWith(2)
    wrapper.unmount()
  })

  it('can change page size', () => {
    const children = jest.fn()
    const onPageSizeChange = jest.fn()
    const wrapper = mount(
      <Pagination
        items={items}
        pageSize={pageSize}
        page={page}
        pageSizeOptions={pageSizeOptions}
        onPageSizeChange={onPageSizeChange}
      >
        {children}
      </Pagination>
    )
    wrapper.find('button.Dropdown').simulate('click')
    wrapper.find('li').at(1).find('a').simulate('click')
    expect(onPageSizeChange).toHaveBeenCalledTimes(1)
    expect(onPageSizeChange).toHaveBeenLastCalledWith(5, 1)
    wrapper.unmount()
  })

  it('can change page when page size changes', () => {
    const children = jest.fn()
    const onPageSizeChange = jest.fn()
    const wrapper = mount(
      <Pagination
        items={items}
        pageSize={pageSize}
        page={lastPage}
        pageSizeOptions={pageSizeOptions}
        onPageSizeChange={onPageSizeChange}
      >
        {children}
      </Pagination>
    )
    wrapper.find('button.Dropdown').simulate('click')
    wrapper.find('li').at(1).find('a').simulate('click')
    expect(onPageSizeChange).toHaveBeenCalledTimes(1)
    expect(onPageSizeChange).toHaveBeenLastCalledWith(5, 6)
    wrapper.unmount()
  })
})

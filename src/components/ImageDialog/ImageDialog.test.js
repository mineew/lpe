import React from 'react'
import { mount } from 'enzyme'
import ImageDialog from './ImageDialog'
import { bookmarkExists } from '../../api'

jest.useFakeTimers()

const imageId = 123
const imageMinWidth = 100
const imageMaxWidth = 1000
const imageMinHeight = 100
const imageMaxHeight = 1000
const isOpen = true

describe('<ImageDialog />', () => {
  it('can change and copy url', (done) => {
    const wrapper = mount(
      <ImageDialog
        imageId={imageId}
        imageMinWidth={imageMinWidth}
        imageMaxWidth={imageMaxWidth}
        imageMinHeight={imageMinHeight}
        imageMaxHeight={imageMaxHeight}
        isOpen={isOpen}
      />
    )
    const copyBtn = wrapper.find('.ImageDialog-copy-btn').first()
    copyBtn.simulate('click')
    expect.assertions(2)
    setImmediate(() => {
      expect(wrapper.state('copied')).toBeTruthy()
      const input = wrapper.find('input').first()
      input.simulate('change', { target: { value: `${imageMinWidth + 10}` } })
      expect(wrapper.state('copied')).toBeFalsy()
      jest.clearAllTimers()
      wrapper.unmount()
      done()
    })
  })

  it('hides the copied info message after timeout', (done) => {
    const wrapper = mount(
      <ImageDialog
        imageId={imageId}
        imageMinWidth={imageMinWidth}
        imageMaxWidth={imageMaxWidth}
        imageMinHeight={imageMinHeight}
        imageMaxHeight={imageMaxHeight}
        isOpen={isOpen}
      />
    )
    const copyBtn = wrapper.find('.ImageDialog-copy-btn').first()
    copyBtn.simulate('click')
    expect.assertions(2)
    setImmediate(() => {
      expect(wrapper.state('copied')).toBeTruthy()
      jest.runAllTimers()
      expect(wrapper.state('copied')).toBeFalsy()
      wrapper.unmount()
      done()
    })
  })

  it('can add images to bookmarks', () => {
    localStorage.clear()
    const wrapper = mount(
      <ImageDialog
        imageId={imageId}
        imageMinWidth={imageMinWidth}
        imageMaxWidth={imageMaxWidth}
        imageMinHeight={imageMinHeight}
        imageMaxHeight={imageMaxHeight}
        isOpen={isOpen}
      />
    )
    const bookmarkBtn = wrapper.find('.ImageDialog-bookmark-btn').first()
    bookmarkBtn.simulate('click')
    expect(bookmarkExists(imageId)).toBeTruthy()
    wrapper.unmount()
  })

  it('is closable', () => {
    const onClose = jest.fn()
    const wrapper = mount(
      <ImageDialog
        imageId={imageId}
        imageMinWidth={imageMinWidth}
        imageMaxWidth={imageMaxWidth}
        imageMinHeight={imageMinHeight}
        imageMaxHeight={imageMaxHeight}
        isOpen={isOpen}
        onClose={onClose}
      />
    )
    const closeBtn = wrapper.find('button').first()
    closeBtn.simulate('click')
    expect(onClose).toHaveBeenCalledTimes(1)
  })
})

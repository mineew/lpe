import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { Dialog, Button, AnchorButton, Classes } from '@blueprintjs/core'
import copy from 'clipboard-copy'
import Image from '../Image/Image'
import ImageUrlForm from '../ImageUrlForm/ImageUrlForm'
import { buildUrl, addToBookmarks, bookmarkExists } from '../../api'
import './ImageDialog.css'

const PADDING = 20
const IMAGE_HOST_HEIGHT = 280
const COPIED_INFO_TIMEOUT = 1000

/**
 * @typedef {Object} ImageDialogProps
 * @property {number} imageId
 * @property {number} imageMinWidth
 * @property {number} imageMaxWidth
 * @property {number} imageMinHeight
 * @property {number} imageMaxHeight
 * @property {boolean} isOpen
 * @property {number} [width]
 * @property {function} [onClose]
 */

/**
 * @augments {Component<ImageDialogProps>} props
 */
export default class ImageDialog extends Component {
  state = {
    url: buildUrl({
      id: this.props.imageId,
      width: this.getImageHostWidth(),
      height: this.getImageHostHeight()
    }),
    copied: false,
    inBookmarks: bookmarkExists(this.props.imageId)
  }

  render () {
    const {
      imageId,
      imageMinWidth, imageMaxWidth, imageMinHeight, imageMaxHeight,
      isOpen, width, onClose
    } = this.props
    const { url, copied, inBookmarks } = this.state
    return (
      <Dialog
        className="ImageDialog"
        icon="media"
        title={`Image #${imageId}`}
        isOpen={isOpen}
        style={{ width }}
        onClose={onClose}
        usePortal={false}
      >
        <div className={Classes.DIALOG_BODY}>
          <div className="ImageDialog-content">
            <div className="ImageDialog-column">
              <Image
                key={url}
                src={url}
                hostWidth={this.getImageHostWidth()}
                hostHeight={this.getImageHostHeight()}
              />
            </div>
            <div className="ImageDialog-column">
              <ImageUrlForm
                imageId={imageId}
                minWidth={imageMinWidth}
                maxWidth={imageMaxWidth}
                defaultWidth={this.getImageHostWidth()}
                minHeight={imageMinHeight}
                maxHeight={imageMaxHeight}
                defaultHeight={this.getImageHostHeight()}
                onChange={(url) => this.setState({ url, copied: false })}
              />
            </div>
          </div>
        </div>
        <div className={Classes.DIALOG_FOOTER}>
          <div className={Classes.DIALOG_FOOTER_ACTIONS}>
            {copied && (
              <Button
                className="ImageDialog-copied-info"
                minimal={true}
                disabled={true}
                icon="tick"
              >
                copied
              </Button>
            )}
            <Button
              className="ImageDialog-copy-btn"
              icon="duplicate"
              intent="primary"
              onClick={this.handleCopy}
            >
              Copy URL to clipboard
            </Button>
            <AnchorButton
              target="_blank"
              icon="document-open"
              href={url}
            >
              Open in new tab
            </AnchorButton>
            <Button
              className="ImageDialog-bookmark-btn"
              icon="bookmark"
              onClick={this.handleBookmark}
              disabled={inBookmarks}
            >
              {!inBookmarks ? 'Add to bookmarks' : 'Added to bookmarks'}
            </Button>
          </div>
        </div>
      </Dialog>
    )
  }

  handleCopy = () => {
    copy(this.state.url).then(() => {
      this.setState({ copied: true })
      setTimeout(() => this.setState({ copied: false }), COPIED_INFO_TIMEOUT)
    })
  }

  handleBookmark = () => {
    addToBookmarks(this.props.imageId)
    this.setState({ inBookmarks: true })
  }

  getImageHostWidth () {
    return this.props.width / 2 - PADDING
  }

  getImageHostHeight () {
    return IMAGE_HOST_HEIGHT
  }
}

ImageDialog.defaultProps = {
  width: 800
}

ImageDialog.propTypes = {
  imageId: PropTypes.number.isRequired,
  imageMinWidth: PropTypes.number.isRequired,
  imageMaxWidth: PropTypes.number.isRequired,
  imageMinHeight: PropTypes.number.isRequired,
  imageMaxHeight: PropTypes.number.isRequired,
  isOpen: PropTypes.bool.isRequired,
  onClose: PropTypes.func
}

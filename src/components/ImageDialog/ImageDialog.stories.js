import React from 'react'
import { storiesOf } from '@storybook/react'
import { number, boolean } from '@storybook/addon-knobs'
import { Callout, Code } from '@blueprintjs/core'
import ImageDialog from './ImageDialog'

const stories = storiesOf('Components', module)

const defaultImageId = 1
const defaultImageMinWidth = 100
const defaultImageMaxWidth = 1000
const defaultImageMinHeight = 100
const defaultImageMaxHeight = 1000
const defaultIsOpen = true
const defaultWidth = ImageDialog.defaultProps.width

stories.add('ImageDialog', () => {
  const imageIdProp = number('imageId', defaultImageId)
  const imageMinWidthProp = number('imageMinWidth', defaultImageMinWidth)
  const imageMaxWidthProp = number('imageMaxWidth', defaultImageMaxWidth)
  const imageMinHeightProp = number('imageMinHeight', defaultImageMinHeight)
  const imageMaxHeightProp = number('imageMaxHeight', defaultImageMaxHeight)
  const isOpenProp = boolean('isOpen', defaultIsOpen)
  const widthProp = number('width', defaultWidth)
  if (!_imageIdIsOk(imageIdProp)) {
    return (
      <Callout intent="danger">
        The <Code>imageId</Code> prop is incorrect.
      </Callout>
    )
  }
  if (!_sizesAreOk({
    imageMinWidth: imageMinWidthProp,
    imageMaxWidth: imageMaxWidthProp,
    imageMinHeight: imageMinHeightProp,
    imageMaxHeight: imageMaxHeightProp
  })) {
    return (
      <Callout intent="danger">
        The sizes of an image are incorrect.
      </Callout>
    )
  }
  return (
    <ImageDialog
      key={imageIdProp}
      imageId={imageIdProp}
      imageMinWidth={imageMinWidthProp}
      imageMaxWidth={imageMaxWidthProp}
      imageMinHeight={imageMinHeightProp}
      imageMaxHeight={imageMaxHeightProp}
      isOpen={isOpenProp}
      width={widthProp}
    />
  )
})

/// ----------------------------------------------------------------------------

function _imageIdIsOk (imageId) {
  return Number.isInteger(imageId) && imageId >= 0
}

function _sizesAreOk (sizes) {
  return Number.isInteger(sizes.imageMinWidth) && sizes.imageMinWidth > 0 &&
         Number.isInteger(sizes.imageMaxWidth) && sizes.imageMaxWidth > 0 &&
         Number.isInteger(sizes.imageMinHeight) && sizes.imageMinHeight > 0 &&
         Number.isInteger(sizes.imageMaxHeight) && sizes.imageMaxHeight > 0
}

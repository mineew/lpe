import React from 'react'
import { storiesOf } from '@storybook/react'
import { action } from '@storybook/addon-actions'
import { object, number } from '@storybook/addon-knobs'
import { Callout, Code } from '@blueprintjs/core'
import Navbar from './Navbar'

const stories = storiesOf('Components', module)

const defaultViews = [{
  id: 1,
  title: 'Charts',
  icon: 'chart',
  count: 11
}, {
  id: 2,
  title: 'Databases',
  icon: 'database',
  count: 10
}, {
  id: 3,
  title: 'Comments',
  icon: 'chat',
  count: 121
}]
const defaultSelectedViewId = 2

stories.add('Navbar', () => {
  const viewsProp = object('views', defaultViews)
  const selectedViewIdProp = number('selectedViewId', defaultSelectedViewId)
  if (!_viewsAreOk(viewsProp)) {
    return (
      <Callout intent="danger">
        The <Code>views</Code> prop is incorrect.
      </Callout>
    )
  }
  if (!_selectedViewIdIsOk(viewsProp, selectedViewIdProp)) {
    return (
      <Callout intent="danger">
        The <Code>selectedViewId</Code> prop is incorrect.
      </Callout>
    )
  }
  return (
    <Navbar
      views={viewsProp}
      selectedViewId={selectedViewIdProp}
      onViewChange={action('onViewChange')}
      onHelp={action('onHelp')}
    />
  )
})

/// ----------------------------------------------------------------------------

function _viewsAreOk (views) {
  let ids = []
  for (let v of views) {
    if (
      !Number.isInteger(v.id) ||
      !(typeof v.title === 'string') ||
      !(typeof v.icon === 'string') ||
      !Number.isInteger(v.count) ||
      ids.includes(v.id)
    ) {
      return false
    }
    ids.push(v.id)
  }
  return true
}

function _selectedViewIdIsOk (views, viewId) {
  for (let v of views) {
    if (v.id === viewId) {
      return true
    }
  }
  return false
}

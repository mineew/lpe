import React, { Component, Fragment } from 'react'
import PropTypes from 'prop-types'
import {
  Navbar as BPNavbar,
  Alignment,
  Button,
  Tag
} from '@blueprintjs/core'
import logo from './logo.png'
import './Navbar.css'

/**
 * @typedef {Object} NavbarView
 * @property {number} id
 * @property {string} title
 * @property {string} icon
 * @property {number} count
 */

/**
 * @typedef {Object} NavbarProps
 * @property {Array.<NavbarView>} views
 * @property {number} selectedViewId
 * @property {function(number)} onViewChange
 * @property {function} [onHelp]
 */

/**
 * @augments {Component<NavbarProps>}
 */
export default class Navbar extends Component {
  render () {
    const { views, selectedViewId, onHelp } = this.props
    return (
      <BPNavbar className="Navbar" fixedToTop={true}>
        <BPNavbar.Group>
          <a
            href={`#${views[0].id}`}
            className="Navbar-logo"
            onClick={this.handleLogoClick}
          >
            <img src={logo} alt="logo" />
          </a>
        </BPNavbar.Group>
        <BPNavbar.Group align={Alignment.RIGHT}>
          {views.map((view) => (
            <Button
              key={view.id}
              className="Navbar-button"
              icon={view.icon}
              active={selectedViewId === view.id}
              onClick={() => this.handleBtnClick(view.id)}
              minimal={true}
              disabled={!view.count}
            >
              {view.title} {view.count > 0 && (
                <Tag round={true}>{view.count}</Tag>
              )}
            </Button>
          ))}
          {onHelp && (
            <Fragment>
              <BPNavbar.Divider />
              <Button
                icon="help"
                minimal={true}
                onClick={onHelp}
              />
            </Fragment>
          )}
        </BPNavbar.Group>
      </BPNavbar>
    )
  }

  handleLogoClick = (e) => {
    e.preventDefault()
    const { onViewChange, views } = this.props
    onViewChange(views[0].id)
  }

  handleBtnClick = (viewId) => {
    const { onViewChange } = this.props
    onViewChange(viewId)
  }
}

const navbarView = PropTypes.shape({
  id: PropTypes.number.isRequired,
  title: PropTypes.string.isRequired,
  icon: PropTypes.string.isRequired,
  count: PropTypes.number.isRequired
})

Navbar.propTypes = {
  views: PropTypes.arrayOf(navbarView),
  selectedViewId: PropTypes.number.isRequired,
  onViewChange: PropTypes.func.isRequired,
  onHelp: PropTypes.func
}

import React from 'react'
import { mount } from 'enzyme'
import Navbar from './Navbar'

const views = [{
  id: 1,
  title: 'View 1',
  icon: 'media',
  count: 1
}, {
  id: 2,
  title: 'View 2',
  icon: 'bookmark',
  count: 2
}]

describe('<Navbar />', () => {
  it('changes a view', () => {
    const onViewChange = jest.fn()
    const wrapper = mount(
      <Navbar
        views={views}
        selectedViewId={views[1].id}
        onViewChange={onViewChange}
      />
    )
    wrapper.find('button').first().simulate('click')
    expect(onViewChange).toHaveBeenCalledTimes(1)
    expect(onViewChange).toHaveBeenLastCalledWith(views[0].id)
    wrapper.find('button').last().simulate('click')
    expect(onViewChange).toHaveBeenCalledTimes(2)
    expect(onViewChange).toHaveBeenLastCalledWith(views[1].id)
    wrapper.unmount()
  })

  it('changes a view when logo is clicked', () => {
    const onViewChange = jest.fn()
    const wrapper = mount(
      <Navbar
        views={views}
        selectedViewId={views[1].id}
        onViewChange={onViewChange}
      />
    )
    wrapper.find('a').simulate('click', { preventDefault: jest.fn() })
    expect(onViewChange).toHaveBeenCalledTimes(1)
    expect(onViewChange).toHaveBeenLastCalledWith(views[0].id)
    wrapper.unmount()
  })

  it('renders the help button', () => {
    const onViewChange = jest.fn()
    const onHelp = jest.fn()
    const wrapper = mount(
      <Navbar
        views={views}
        selectedViewId={views[1].id}
        onViewChange={onViewChange}
        onHelp={onHelp}
      />
    )
    wrapper.find('button').last().simulate('click')
    expect(onHelp).toHaveBeenCalledTimes(1)
    wrapper.unmount()
  })

  it('doesn\'t change a view with zero values', () => {
    views[1].count = 0
    const onViewChange = jest.fn()
    const wrapper = mount(
      <Navbar
        views={views}
        selectedViewId={views[0].id}
        onViewChange={onViewChange}
      />
    )
    wrapper.find('button').last().simulate('click')
    expect(onViewChange).toHaveBeenCalledTimes(0)
    wrapper.unmount()
  })
})

import React from 'react'
import { shallow } from 'enzyme'
import Loader from './Loader'

describe('<Loader />', () => {
  it('can render "loading" or "error" states', () => {
    const wrapper = shallow(<Loader />)
    wrapper.setProps({
      loading: true,
      error: false
    })
    expect(wrapper.hasClass('Loader-loading')).toBeTruthy()
    expect(wrapper.hasClass('Loader-error')).toBeFalsy()
    wrapper.setProps({
      loading: false,
      error: true
    })
    expect(wrapper.hasClass('Loader-loading')).toBeFalsy()
    expect(wrapper.hasClass('Loader-error')).toBeTruthy()
  })

  it('prefers "error" state when both are given', () => {
    const wrapper = shallow(<Loader />)
    wrapper.setProps({
      loading: true,
      error: true
    })
    expect(wrapper.hasClass('Loader-loading')).toBeFalsy()
    expect(wrapper.hasClass('Loader-error')).toBeTruthy()
  })

  it('can render a message', () => {
    const wrapper = shallow(<Loader />)
    expect(wrapper.find('.Loader-message')).toHaveLength(0)
    wrapper.setProps({
      loading: true,
      error: true,
      message: 'Loading Message'
    })
    expect(wrapper.find('.Loader-message').text()).toBe('Loading Message')
    wrapper.setProps({
      loading: false,
      error: true,
      message: 'Error Message'
    })
    expect(wrapper.find('.Loader-message').text()).toBe('Error Message')
  })

  it('renders default message in the "error" state', () => {
    const wrapper = shallow(<Loader error={true} />)
    expect(wrapper.find('.Loader-message').text()).toBeDefined()
  })

  it(
    'renders nothing when there is neither "error" nor "loading" state',
    () => {
      const wrapper = shallow(<Loader loading={false} error={false} />)
      expect(wrapper.html()).toBeNull()
    }
  )
})

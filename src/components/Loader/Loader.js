import React from 'react'
import PropTypes from 'prop-types'
import { Icon, Spinner } from '@blueprintjs/core'
import './Loader.css'

/**
 * @typedef {Object} LoaderProps
 * @property {boolean} [loading]
 * @property {boolean} [error]
 * @property {string} [message]
 */

/**
 * @param {LoaderProps} props
 * @returns {React.Component<LoaderProps>}
 */
export default function Loader (props) {
  if (!props.error && !props.loading) {
    return null
  }
  let icon = <Spinner size={60} />
  if (props.error) {
    icon = <Icon icon="warning-sign" iconSize={40} />
  }
  let message = props.message
  if (!message && props.error) {
    message = 'Something went wrong...'
  }
  return (
    <div className={`Loader Loader-${props.error ? 'error' : 'loading'}`}>
      {icon}
      {message && (
        <div className="Loader-message">
          {message}
        </div>
      )}
    </div>
  )
}

Loader.defaultProps = {
  loading: true
}

Loader.propTypes = {
  loading: PropTypes.bool,
  error: PropTypes.bool,
  message: PropTypes.string
}

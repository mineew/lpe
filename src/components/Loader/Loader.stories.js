import React from 'react'
import { storiesOf } from '@storybook/react'
import { number, boolean, text } from '@storybook/addon-knobs'
import Loader from './Loader'

const stories = storiesOf('Components', module)

const defaultContainerWidth = 250
const defaultContainerHeight = 200

const defaultLoading = true
const defaultError = false
const defaultMessage = ''

stories.add('Loader', () => {
  const width = number('[Container Width]', defaultContainerWidth)
  const height = number('[Container Height]', defaultContainerHeight)
  const loadingProp = boolean('loading', defaultLoading)
  const errorProp = boolean('error', defaultError)
  const messageProp = text('message', defaultMessage)
  return (
    <div style={{ width, height }}>
      <Loader
        loading={loadingProp}
        error={errorProp}
        message={messageProp}
      />
    </div>
  )
})

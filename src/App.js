import React, { Component, Fragment } from 'react'
import {
  Loader, Navbar, Pagination, ImageList, ImageDialog
} from './components'
import { loadImagesDB, buildUrl, getBookmarks } from './api'
import './App.css'

const IMAGE_WIDTH_IN_LIST = 218
const IMAGE_HEIGHT_IN_LIST = 170
const IMAGE_CONFIG_MIN_WIDTH = 100
const IMAGE_CONFIG_MIN_HEIGHT = 100

const PAGE_SIZE_OPTIONS = [10, 20, 30, 40, 50]

const TIMEOUT = 8000
const TIMEOUT_ERROR_MESSAGE =
  'Looks like the server is taking to long to respond...'

export const LOREM_PICSUM_DB_VIEW = {
  id: 1,
  title: 'Lorem Picsum DB',
  icon: 'media',
  count: 0
}
export const BOOKMARKS_VIEW = {
  id: 2,
  title: 'Bookmarks',
  icon: 'bookmark',
  count: 0
}

export default class App extends Component {
  state = {
    images: [],
    loading: true,
    error: null,
    selectedImageId: 0,
    imageDialogIsOpen: false,
    loremPicsumDBPage: 1,
    bookmarksPage: 1,
    loremPicsumDBPageSize: PAGE_SIZE_OPTIONS[1],
    bookmarksPageSize: PAGE_SIZE_OPTIONS[1],
    selectedViewId: LOREM_PICSUM_DB_VIEW.id
  }

  componentDidMount () {
    loadImagesDB(TIMEOUT, TIMEOUT_ERROR_MESSAGE)
      .then((images) => this.setState({
        images: _convertImages(images),
        loading: false
      }))
      .catch((error) => this.setState({
        error,
        loading: false
      }))
  }

  render () {
    const {
      loading, error, images, imageDialogIsOpen, selectedViewId
    } = this.state
    const { page, pageSize, items } = this.getPaginationConfig()
    const bookmarks = _pickFromBookmarks(images)
    LOREM_PICSUM_DB_VIEW.count = images.length
    BOOKMARKS_VIEW.count = bookmarks.length
    return (
      <div className="App">
        {(error || loading) && (
          <div className="App-Loader-wrapper">
            <Loader
              loading={loading}
              error={Boolean(error)}
              message={error && error.message ? error.message : ''}
            />
          </div>
        )}
        {!error && !loading && (
          <Fragment>
            <Navbar
              views={[LOREM_PICSUM_DB_VIEW, BOOKMARKS_VIEW]}
              selectedViewId={selectedViewId}
              onViewChange={this.handleViewChange}
            />
            <Pagination
              items={items}
              page={page}
              pageSize={pageSize}
              pageSizeOptions={PAGE_SIZE_OPTIONS}
              onPageChange={this.handlePageChange}
              onPageSizeChange={this.handlePageSizeChange}
            >
              {(imagesPerPage) => (
                <ImageList
                  images={imagesPerPage}
                  imageWidth={IMAGE_WIDTH_IN_LIST}
                  imageHeight={IMAGE_HEIGHT_IN_LIST}
                  onSelect={this.handleImageSelect}
                />
              )}
            </Pagination>
            <ImageDialog
              key={this.selectedImage.id}
              imageId={this.selectedImage.id}
              imageMaxWidth={this.selectedImage.width}
              imageMinWidth={IMAGE_CONFIG_MIN_WIDTH}
              imageMaxHeight={this.selectedImage.height}
              imageMinHeight={IMAGE_CONFIG_MIN_HEIGHT}
              isOpen={imageDialogIsOpen}
              onClose={this.handleImageDialogClose}
            />
          </Fragment>
        )}
      </div>
    )
  }

  handleViewChange = (selectedViewId) => {
    this.setState({ selectedViewId })
  }

  handlePageChange = (page) => {
    this.setState((prevState) => {
      if (prevState.selectedViewId === LOREM_PICSUM_DB_VIEW.id) {
        return { loremPicsumDBPage: page }
      } else {
        return { bookmarksPage: page }
      }
    })
  }

  handlePageSizeChange = (pageSize, page) => {
    this.setState((prevState) => {
      if (prevState.selectedViewId === LOREM_PICSUM_DB_VIEW.id) {
        return {
          loremPicsumDBPage: page,
          loremPicsumDBPageSize: pageSize
        }
      } else {
        return {
          bookmarksPage: page,
          bookmarksPageSize: pageSize
        }
      }
    })
  }

  handleImageSelect = (selectedImageId) => {
    this.setState({
      selectedImageId,
      imageDialogIsOpen: true
    })
  }

  handleImageDialogClose = () => {
    this.setState({ imageDialogIsOpen: false })
  }

  get selectedImage () {
    const { images, selectedImageId } = this.state
    for (let img of images) {
      if (img.id === selectedImageId) {
        return img
      }
    }
    return null
  }

  getPaginationConfig () {
    const { selectedViewId } = this.state
    if (selectedViewId === LOREM_PICSUM_DB_VIEW.id) {
      return {
        page: this.state.loremPicsumDBPage,
        pageSize: this.state.loremPicsumDBPageSize,
        items: this.state.images
      }
    } else {
      return {
        page: this.state.bookmarksPage,
        pageSize: this.state.bookmarksPageSize,
        items: _pickFromBookmarks(this.state.images)
      }
    }
  }
}

/// ----------------------------------------------------------------------------

/**
 * @private
 * @param {Array} images
 */
function _convertImages (images) {
  return images.map((img) => ({
    ...img,
    src: buildUrl({
      id: img.id,
      width: IMAGE_WIDTH_IN_LIST,
      height: IMAGE_HEIGHT_IN_LIST
    })
  }))
}

/**
 * @private
 * @param {Array} images
 */
function _pickFromBookmarks (images) {
  const bookmarks = getBookmarks()
  const savedImages = []
  for (let imageId of bookmarks) {
    for (let img of images) {
      if (img.id === imageId) {
        savedImages.push(img)
        break
      }
    }
  }
  return savedImages
}

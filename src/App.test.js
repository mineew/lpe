import React from 'react'
import { mount } from 'enzyme'
import * as loremPicsumModule from './api/loremPicsum'
import { addToBookmarks } from './api'
import App, { BOOKMARKS_VIEW } from './App'

const mockData = []
for (let i = 0; i < 100; i++) {
  mockData.push({ id: i, width: 1000, height: 1000 })
}

const mockError = new Error('Mock Error')

let loadImagesDBShouldFail = false
loremPicsumModule.loadImagesDB = function () {
  return {
    then (cb) {
      cb(mockData)
      return {
        catch (cb) {
          if (loadImagesDBShouldFail) {
            cb(mockError)
          } else {
            cb(null)
          }
        }
      }
    }
  }
}

describe('<App />', () => {
  it('renders', () => {
    const wrapper = mount(<App />)
    wrapper.unmount()
  })

  it('renders an error message', () => {
    loadImagesDBShouldFail = true
    const wrapper = mount(<App />)
    expect(wrapper.find('.Loader-message').html()).toMatch(mockError.message)
    wrapper.unmount()
    loadImagesDBShouldFail = false
  })

  it('can render bookmarks', () => {
    addToBookmarks(0)
    addToBookmarks(1)
    addToBookmarks(2)
    const wrapper = mount(<App />)
    wrapper.find('.Navbar-button').last().simulate('click')
    expect(wrapper.state('selectedViewId')).toBe(BOOKMARKS_VIEW.id)
    wrapper.unmount()
  })

  it('manages two paginations', () => {
    for (let i = 0; i < 100; i++) {
      addToBookmarks(i)
    }
    const wrapper = mount(<App />)
    // lorem picsum DB view
    wrapper.find('.Pagination-controls button').at(4).simulate('click')
    expect(wrapper.state('loremPicsumDBPage')).toBe(4)
    wrapper.find('.Pagination-controls .Dropdown').first().simulate('click')
    wrapper.find('li').at(2).find('a').simulate('click')
    expect(wrapper.state('loremPicsumDBPageSize')).toBe(30)
    // bookmarks view
    wrapper.find('.Navbar-button').last().simulate('click')
    wrapper.find('.Pagination-controls button').at(3).simulate('click')
    expect(wrapper.state('bookmarksPage')).toBe(3)
    wrapper.find('.Pagination-controls .Dropdown').first().simulate('click')
    wrapper.find('li').at(3).find('a').simulate('click')
    expect(wrapper.state('bookmarksPageSize')).toBe(40)
    wrapper.unmount()
  })

  it('can open the image dialog', () => {
    const wrapper = mount(<App />)
    wrapper.find('.ImageListItem img').first().simulate('load')
    wrapper.find('.ImageListItem').first().simulate('click')
    expect(wrapper.state('imageDialogIsOpen')).toBeTruthy()
    wrapper.unmount()
  })

  it('can close the image dialog', () => {
    const wrapper = mount(<App />)
    wrapper.find('.ImageListItem img').first().simulate('load')
    wrapper.find('.ImageListItem').first().simulate('click')
    wrapper.find('.ImageDialog .bp3-dialog-close-button').simulate('click')
    expect(wrapper.state('imageDialogIsOpen')).toBeFalsy()
    wrapper.unmount()
  })
})

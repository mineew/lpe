/**
 * @param {number} ms
 * @param {string} errorMessage
 * @param {Promise} promise
 * @returns {Promise}
 */
export function cancelByTimeout (ms, errorMessage, promise) {
  return new Promise((resolve, reject) => {
    setTimeout(() => reject(new Error(errorMessage)), ms)
    promise.then(resolve, reject)
  })
}

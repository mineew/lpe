import { cancelByTimeout } from './utils'

export const API_HOST = 'https://picsum.photos'

/**
 * @typedef {Object} ImageUrlOptions
 * @property {number} width
 * @property {number} height
 * @property {number} [id]
 * @property {boolean} [grayscale]
 * @property {boolean} [blur]
 * @property {('north'|'east'|'south'|'west'|'center')} [gravity]
 */

/**
 * @param {ImageUrlOptions} opts
 */
export function buildUrl (opts) {
  if (!opts.width || !opts.height) {
    throw new Error('Width and height must be defined.')
  }
  let url = opts.grayscale ? `${API_HOST}/g` : `${API_HOST}`
  url = `${url}/${opts.width}/${opts.height}/`
  url = opts.id !== undefined ? `${url}?image=${opts.id}` : `${url}?random`
  url = opts.blur ? `${url}&blur` : url
  url = opts.gravity ? `${url}&gravity=${opts.gravity}` : url
  return url
}

export const GRAVITY_OPTIONS = [
  'north',
  'east',
  'south',
  'west',
  'center'
]

/**
 * @typedef {Object} LoremPicsumImageInfo
 * @property {number} id
 * @property {number} width
 * @property {number} height
 * @property {string} filename
 * @property {string} format
 * @property {string} author
 * @property {string} author_url
 */

/**
 * @param {number} timeout
 * @param {string} timeoutErrorMessage
 * @returns {Promise<Array.<LoremPicsumImageInfo>>}
 */
export function loadImagesDB (timeout, timeoutErrorMessage) {
  const request = cancelByTimeout(
    timeout,
    timeoutErrorMessage,
    fetch(`${API_HOST}/list`)
  )
  return request
    .then((response) => {
      if (response.status !== 200) {
        throw new Error(`[${response.status}]: ${response.statusText}`)
      }
      return response.json()
    })
}

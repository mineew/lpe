import { cancelByTimeout } from './utils'

describe('cancelByTimeout', () => {
  it('rejects promise by timeout', () => {
    const ms = 100
    let promise = new Promise((resolve) => {
      setTimeout(() => resolve(), ms + 100)
    })
    expect.assertions(1)
    return expect(
      cancelByTimeout(ms, 'Timeout Error', promise)
    ).rejects.toBeInstanceOf(Error)
  })
})

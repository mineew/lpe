import { getBookmarks, bookmarkExists, addToBookmarks } from './bookmarks'

beforeEach(() => {
  localStorage.clear()
})

describe('addToBookmarks', () => {
  it('adds a bookmark', () => {
    expect(bookmarkExists(1)).toBeFalsy()
    addToBookmarks(1)
    expect(bookmarkExists(1)).toBeTruthy()
  })

  it('doesn\'t add twice', () => {
    addToBookmarks(1)
    addToBookmarks(1)
    addToBookmarks(1)
    expect(getBookmarks()).toEqual([1])
  })
})

describe('bookmarkExists', () => {
  it('checks if a bookmark is exist', () => {
    addToBookmarks(1)
    addToBookmarks(2)
    expect(bookmarkExists(1)).toBeTruthy()
    expect(bookmarkExists(2)).toBeTruthy()
    expect(bookmarkExists(3)).toBeFalsy()
  })
})

describe('getBookmarks', () => {
  it('gets a list of the bookmarks', () => {
    addToBookmarks(1)
    addToBookmarks(2)
    addToBookmarks(3)
    expect(getBookmarks()).toHaveLength(3)
    expect(getBookmarks()).toEqual([1, 2, 3])
  })
})

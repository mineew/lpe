const LOCAL_STORAGE_KEY = 'bookmarks'

/**
 * @returns {Array.<number>}
 */
export function getBookmarks () {
  return localStorage.getItem(LOCAL_STORAGE_KEY)
    ? JSON.parse(localStorage.getItem(LOCAL_STORAGE_KEY))
    : []
}

/**
 * @param {number} imageId
 * @returns {boolean}
 */
export function bookmarkExists (imageId) {
  let bookmarks = getBookmarks()
  return bookmarks.some((id) => id === imageId)
}

/**
 * @param {number} imageId
 * @returns {Array.<number>}
 */
export function addToBookmarks (imageId) {
  let bookmarks = getBookmarks()
  if (!bookmarkExists(imageId)) {
    bookmarks.push(imageId)
    localStorage.setItem(LOCAL_STORAGE_KEY, JSON.stringify(bookmarks))
  }
}

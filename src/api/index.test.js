import * as apiModule from '.'

describe('api module', () => {
  it('exports desired set of functions', () => {
    expect(apiModule.buildUrl).toBeDefined()
    expect(apiModule.loadImagesDB).toBeDefined()
    expect(apiModule.GRAVITY_OPTIONS).toBeDefined()
    expect(apiModule.getBookmarks).toBeDefined()
    expect(apiModule.addToBookmarks).toBeDefined()
    expect(apiModule.bookmarkExists).toBeDefined()
  })
})

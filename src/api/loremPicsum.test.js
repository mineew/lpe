import { buildUrl, loadImagesDB } from './loremPicsum'

describe('buildUrl', () => {
  it('throws when `width` or `height` are falsy', () => {
    expect.assertions(3)
    expect(buildUrl).toThrow()
    try {
      buildUrl({ width: 100 })
    } catch (e) {
      expect(e).toBeDefined()
    }
    try {
      buildUrl({ height: 100 })
    } catch (e) {
      expect(e).toBeDefined()
    }
  })

  it('by default returns the url to a random image', () => {
    const url = buildUrl({ width: 150, height: 100 })
    expect(url).toBe('https://picsum.photos/150/100/?random')
  })

  it('can build urls to specific images', () => {
    const url = buildUrl({ width: 150, height: 100, id: 197 })
    expect(url).toMatch('image=197')
  })

  it('can build urls to grayscale images', () => {
    const url = buildUrl({ width: 150, height: 100, grayscale: true })
    expect(url).toMatch('https://picsum.photos/g/')
  })

  it('can build urls to blur images', () => {
    const url = buildUrl({ width: 150, height: 100, blur: true })
    expect(url).toMatch('blur')
  })

  it('can crop images', () => {
    const url = buildUrl({ width: 150, height: 100, gravity: 'north' })
    expect(url).toMatch('gravity=north')
  })
})

describe('loadImagesDB', () => {
  it('cancels by timeout', () => {
    const timeout = 100
    const errorMessage = 'Timeout Error'
    global.fetchDelay = 200
    expect.assertions(1)
    return expect(
      loadImagesDB(timeout, errorMessage)
    ).rejects.toHaveProperty('message', errorMessage)
  })

  it('can handle response statutes other than 200', () => {
    const timeout = 10000
    const timeoutErrorMessage = 'Timeout Error'
    global.fetchDelay = 0
    global.fetchStatus = 500
    global.fetchStatusText = 'Internal Error'
    expect.assertions(1)
    return expect(
      loadImagesDB(timeout, timeoutErrorMessage)
    ).rejects.toHaveProperty(
      'message',
      `[${global.fetchStatus}]: ${global.fetchStatusText}`
    )
  })

  it('resolves with an array of objects', async () => {
    const timeout = 10000
    const timeoutErrorMessage = 'Timeout Error'
    global.fetchStatus = 200
    expect.assertions(2)
    const data = await loadImagesDB(timeout, timeoutErrorMessage)
    expect(data).toBeInstanceOf(Array)
    expect(data[0]).toHaveProperty('id')
  })
})
